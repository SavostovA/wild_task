#Using a template to pass freshly created i_am keys
resource "template_file" "user_data_management" {
  template = "${file("${var.bootstrap_management_path}")}"
  vars = {
    access = "${aws_iam_access_key.adi.id}"
    secret = "${aws_iam_access_key.adi.secret}"
  }
}

resource "aws_instance" "Management_master" {
  count                  = 1
  ami                    = "${var.ami}"
  instance_type          = "${var.instance_type}"
  key_name               = "${var.key_name}"
  user_data              = "${template_file.user_data_management.rendered}"
  vpc_security_group_ids = ["${aws_security_group.k8s.id}"]
  tags = {
    Name = "Management_master"
  }
}

resource "aws_instance" "k8s-master" {
  ami                    = "${var.ami}"
  instance_type          = "${var.instance_type}"
  key_name               = "${var.key_name}"
  user_data              = file("${var.bootstrap_k8s_path}")
  vpc_security_group_ids = ["${aws_security_group.k8s.id}"]
  tags = {
    Name = "k8s_master"
  }
}

resource "aws_instance" "k8s_node" {
  count                  = 2
  ami                    = "${var.ami}"
  instance_type          = "${var.instance_type}"
  key_name               = "${var.key_name}"
  user_data              = file("${var.bootstrap_k8s_path}")
  vpc_security_group_ids = ["${aws_security_group.k8s.id}"]
  tags = {
    Name = "k8s_node_${count.index}"
  }
}
